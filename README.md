# zenslack
A Slack bot that will use a slash command to create tickets in Zendesk


###Config file

```
# config.py
zendesk_url = "Your Zendesk main url"
zendesk_email = "Your primary zendesk email address"
zendesk_api_token = "Your Zendesk API Token"
slack_webhook_token = "Your Slack outgoing webhook token"
slack_api_token = "Slack API token with users:read scope"

```