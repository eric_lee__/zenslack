import config
import requests
from flask import Flask, request
from zdesk import Zendesk, get_id_from_url
from threading import Thread
app = Flask(__name__)


@app.route('/', methods=['POST'])
def listen_for_slack_webhooks():
    if request.form.get('token') == config.slack_webhook_token:
        user_id = request.form.get('user_id')
        text = request.form.get('text')
        url = request.form.get('response_url')
        # Created thread so that return would happen in less than three seconds
        slack_response_thread = Thread(target=convert_webhook_to_ticket, args=(user_id, text, url))
        slack_response_thread.start()
        return "Creating ticket...", 200
    else:
        return '', 401


# helper functions
def lookup_slack_user(id):
    api_token = config.slack_api_token
    payload = {'token': api_token, 'user': id}
    raw_slack_user = requests.post('https://slack.com/api/users.info', data=payload)
    user = raw_slack_user.json()
    return user


def create_zendesk_ticket(name, email, ticket_subject):    
    zurl = config.zendesk_url
    zemail = config.zendesk_email
    ztoken = config.zendesk_api_token
    zendesk = Zendesk(zurl, zemail, ztoken, True)
    new_ticket = {
        'ticket': {
            'requester': {
                'name': name,
                'email': email,
                },
            'subject': ticket_subject,
            'description': ticket_subject
            }
        }
    try:
        ticket_url = zendesk.ticket_create(data=new_ticket)
        ticket_id = get_id_from_url(ticket_url)
        return ticket_id
    except Exception as e:
        print(e)
        return None


# Main function
def convert_webhook_to_ticket(user_id, ticket_subject, response_url):
    user_data = lookup_slack_user(user_id)
    real_name = user_data['user']['profile']['real_name']
    email = user_data['user']['profile']['email']
    ticket_num = create_zendesk_ticket(real_name, email, ticket_subject)
    if ticket_num is None:
        requests.post(response_url, json={'text': "There was an error creating your ticket"})
    else:
        slack_slash_response = {
            'text': "Your ticket ID number is {}."
                    " You should receive a verification email to {} within five minutes".format(ticket_num, email)
        }
        response = requests.post(response_url, json=slack_slash_response)
        return response


if __name__ == "__main__":
    app.run()
